const express = require("express");

const app = express.Router();
const URL = require("../Model/urlModel");

app.get("/getgenerateUrl", async (req, res) => {
  const urllist = await URL.find();
  if (urllist) {
    return res.send(urllist);
  }
  
});
app.post("/generateUrl", async (req, res) => {
  console.log(req.body);
  var Link = new URL({
    url: req.body.url,
  });

  const Links = Link.save();
  if (Links) {
    res.send({
      status: "saved",
    });
  }
  if (!Links) {
    res.send({
      status: "not saved",
    });
  }
});

app.get("/:ShortID", async (req, res) => {
  const ShortID = req.params.ShortID;

  const redirect = await URL.findOne({ ShortID });

if(redirect==null){
  res.send("Invalid Url ")
}
if(redirect){
  console.log(redirect.url);
  res.redirect(redirect.url);
}
  
});

module.exports = app;
