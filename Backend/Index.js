const express = require("express");
const app = express();

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const mongoose = require("./db");
const cors = require("cors");
const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

// middleWare Routes

const ShortenURl = require("./Controller/shortenUrl");

app.listen(4300, () => {
  console.log("running on port 4300");
});

// app use

app.use("", ShortenURl);
