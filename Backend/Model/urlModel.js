const mongoose = require("mongoose");
const shortid = require("shortid");

const URl = mongoose.model("urlLinks", {
  url: {
    type: String,
    required: true,
  },
  ShortID: {
    type: String,
    required: true,
    default: shortid.generate,
  },
});

module.exports = URl;
