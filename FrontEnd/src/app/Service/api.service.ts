import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

   url="http://localhost:4300"
   generateURL(data:any){
    console.log(data)
    return this.http.post(this.url + "/generateUrl", data)
  }
   

  getUrlList(){
    return this.http.get(this.url+"/getgenerateUrl")
  }
  RedirectURL(ShortID:any){
    console.log(ShortID)
    return this.http.get(this.url+"/"+ShortID)
  }
  

}
