import { Component, OnInit } from '@angular/core';
import { ApiService } from './Service/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'shortenURL';

  url: any

  UrlList: any;
  constructor(private api: ApiService) {

  }

  ngOnInit(): void {
    this.getList()
  }
  getUrl(data: any) {

    const values = data.value
    this.url = values
    let url1 = { url: this.url }
    console.log(url1);
    this.api.generateURL(url1).subscribe((Res) => {

      console.log(Res);
    })

  }
  async getList() {
    (await this.api.getUrlList()).subscribe((res: any) => {
      console.log(res)
      this.UrlList = res

    })
  }
  route(ShortID: any) {
    this.redirect(ShortID)
    console.log(ShortID);
  }

  async redirect(ShortID: any) {
    (await this.api.RedirectURL(ShortID)).subscribe((res: any) => {
      console.log(res)
      alert(res)


    })
  }

}

